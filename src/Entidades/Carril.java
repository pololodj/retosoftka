package Entidades;

import Carga.id.com.Idcarro;
import Carga.id.com.Idjuego;
import Cargar.Posicion.Posicion;

public class Carril {
	
	 protected Idcarro carroId;
	    protected Idjuego juegoId;
	    protected Posicion posicion;
	    protected Integer metros;
	    protected Boolean desplazamientoFinal;

	    public Carril(Idcarro carroId, Idjuego juegoId, Posicion posicion, Integer metros, Boolean desplazamientoFinal) {
	        this.carroId = carroId;
	        this.juegoId = juegoId;
	        this.posicion = posicion;
	        this.metros = metros;
	        this.desplazamientoFinal = desplazamientoFinal;
	    }

	    public void alcanzarLaMeta() {
	        if (posicionActual() >= posicionDeseada()) {
	            desplazamientoFinal = true;
	        }

	    }

	    public void moverCarro(Posicion posicion, Integer cantidad) {
	        this.posicion = posicion;
	        posicion.setActual(posicion.actual() + cantidad);
	        alcanzarLaMeta();
	    }

	    public Integer metros() {

	        return metros;

	    }

	    public Posicion posicion() {

	        return posicion;

	    }

	    public Integer posicionActual() {

	        return posicion.actual();

	    }

	    public Integer posicionDeseada() {
	        return posicion.meta();
	    }

	    public Boolean desplazamientoFinal() {

	        return desplazamientoFinal;

	    }


}
