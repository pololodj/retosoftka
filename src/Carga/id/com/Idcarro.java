package Carga.id.com;

import java.util.UUID;

public class Idcarro {
    private UUID id;

    public Idcarro(UUID id) {
        this.id = id;
    }

    public String getId() {
        return id.toString();
    }

}
