package com.cargar.juego;

import Carga.id.com.Nombre;


public class Jugador {
	private Nombre nombre;
    private Integer puntos;

    public Jugador(Nombre nombre, Integer puntos) {
        this.nombre = nombre;
        this.puntos = puntos;
    }

    public Nombre nombre() {
        return nombre;
    }

   
    public void asignarPuntos(Integer puntos) {
        puntos = puntos;
    }

}
