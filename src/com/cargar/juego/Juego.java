package com.cargar.juego;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.UUID;

import javax.swing.JOptionPane;

import Carga.id.com.Idcarro;
import Carga.id.com.Idjuego;
import Carga.id.com.Idjugador;
import Carga.id.com.Nombre;
import Cargar.Posicion.Posicion;
import Cargar.carro.Carro;
import Cargar.carro.Conductor;
import Entidades.Carril;
import co.com.cargar.pista.Pista;
import co.com.cargar.pista.Podio;

public class Juego {
	 public Juego() {
	    }

	    protected Map<Idjugador, Jugador> jugadores = new HashMap<>();
	    protected Pista pista;
	    protected Boolean jugando;
	    protected Podio podio = new Podio();
	    protected ArrayList<Pista> pistas = new ArrayList<>();
	    protected ArrayList<Carro> carrosEnJuego = new ArrayList<>();
	    protected ArrayList<Carril> carrilesEnJuego = new ArrayList<>();
	    private final Carro carro = new Carro();
	    private Boolean primeraPartida=true;

	 

	    public void crearJugador(Idjugador jugadorId, Nombre nombre) {
	        Jugador jugador = new Jugador(nombre, 0);
	        jugadores.put(jugadorId, jugador);
	        crearConductor(nombre);
	    }

	    public void crearConductor(Nombre nombre) {
	        UUID id;            
	        Scanner in = new Scanner(System.in);
	        JOptionPane.showMessageDialog(null, "Eljugador : "+nombre.getNombre()+" Quieres ser Conductor en esta Partida "+" Y/N ");
	        while (!in.hasNext("[yYnN]")) {
	            System.out.println("Solo se reciben como respuesta Y/N � y/n");
	            in.next();
	        }
	        String consultaConductores = in.next();
	        if (consultaConductores.equals("Y") || consultaConductores.equals("y")) {
	            Conductor conductor = new Conductor(nombre.getNombre());
	            id = UUID.randomUUID();
	            Idcarro carroId = new Idcarro(id);
	            carro.asignarConductor(carroId, conductor);
	        }
	    }

	    public void crearPistas() {
	        int kilometrosRandom;
	        int numeroCarriles = carro.numeroCarros();
	        for (int i = 0; i < carro.numeroCarros(); i++) {
	            kilometrosRandom = (int) (Math.random() * 100 + 1);
	            Pista pista = new Pista(kilometrosRandom, numeroCarriles);
	            pistas.add(pista);
	        }
	    }

	    public void asignarPrimerLugar(Idjugador jugadorId) {
	        podio.asignarPrimerLugar(jugadores.get(jugadorId));
	        JOptionPane.showMessageDialog(null, "En el Primer lugar : "+jugadores.get(jugadorId).nombre().getNombre());

	    }

	    public void asignarSegundoLugar(Idjugador jugadorId) {
	        podio.asignarSegundoLugar(jugadores.get(jugadorId));
	        JOptionPane.showMessageDialog(null, "En el Segundo lugar : "+jugadores.get(jugadorId).nombre().getNombre());

	    }

	    public void asignarTercerLugar(Idjugador jugadorId) {
	        podio.asignarTercerLugar(jugadores.get(jugadorId));
	        JOptionPane.showMessageDialog(null, "En el Tercer lugar : "+jugadores.get(jugadorId).nombre().getNombre());

	        System.out.println("**********" + jugadores.get(jugadorId).nombre().getNombre() + ": Tercer Lugar" + "***********");

	    }

	    public void iniciarJuego() {
	        UUID id;
	        id = UUID.randomUUID();
	        Idjuego juegoId = new Idjuego(id);
	        Scanner in = new Scanner(System.in);
	        System.out.println("Para iniciar el juego, elige la  pista (numero) en la que deseas jugar:  ");
	        System.out.println("Pistas: ");
	        int counter = 1;
	        for (Pista p : pistas) {
	            System.out.println(counter + "." + " Kilometros: " + p.kilometros() + " N�mero de carriles:  " + p.numeroDeCarriles());
	            counter++;
	        }
	        
	       while(!in.hasNextInt()) in.next();   
	        int pistaElegida = in.nextInt();
	        carro.carros().forEach((key, value) -> {
	            Carro carrosJuego = new Carro(value, 0,  juegoId);
	            carrosEnJuego.add(carrosJuego);
	            int kilometrosToMetros = pistas.get(pistaElegida - 1).kilometros() * 1000;
	            Posicion posicion = new Posicion(0, kilometrosToMetros);
	            Carril carriles = new Carril(key, juegoId, posicion, kilometrosToMetros, false);
	            carrilesEnJuego.add(carriles);
	        });
	        jugando = true;
	        Conductor conductor = new Conductor();
	        System.out.println(" ****** Inicia la carrera ******** ");
	        while (jugando) {
	            int contador = 0;
	            System.out.println(" ****** Avance *****  " + "****  Meta: " + carrilesEnJuego.get(contador).metros() + " metros");
	            for (Carro carros : carrosEnJuego) {
	                if (!yaGanoCarro(carros.conductor().nombre())) {
	                    int mover = conductor.lanzarDado() * 100;
	                    carros.setDistancia(carros.distancia() + mover);
	                    carrilesEnJuego.get(contador).moverCarro(carrilesEnJuego.get(contador).posicion(), mover);
	                    System.out.println(carros.conductor().nombre() + ":" + " mueve: " + mover + " Nueva posici�n: " + carros.distancia());
	                    if (carrilesEnJuego.get(contador).desplazamientoFinal()) {
	                        if (podio.primerLugar() == null) {
	                            asignarPrimerLugar(jugadorID(carros.conductor().nombre()));
	                        } else if (podio.segundoLugar() == null) {
	                            asignarSegundoLugar(jugadorID(carros.conductor().nombre()));
	                        } else if (podio.tercerLugar() == null) {
	                            asignarTercerLugar(jugadorID(carros.conductor().nombre()));
	                        }
	                    }
	                }
	                contador++;
	            }
	            if (podio.estaLleno()) {
	                break;
	            }
	        }

	        mostrarPodio();
	        repetirJuego();
	    }

	    public Map<Idjugador, Jugador> jugadores() {

	        return jugadores;

	    }

	    public Boolean jugando() {

	        return jugando;

	    }

	       
	    public Idjugador jugadorID(String nombre) {
	        Idjugador lookId = null;
	        for (Idjugador keys : jugadores.keySet()) {
	            if (jugadores.get(keys).nombre().getNombre().equals(nombre)) {
	                lookId = keys;
	            }
	        }
	        return lookId;
	    }
	    public Boolean yaGanoCarro(String nombre) {
	        boolean yaGano = false;
	        if (podio.tercerLugar() == jugadores.get(jugadorID(nombre))
	                || podio.primerLugar() == jugadores.get(jugadorID(nombre))
	                || podio.segundoLugar() == jugadores.get(jugadorID(nombre))) {
	            yaGano = true;
	        }
	        return yaGano;
	    }

	    

	    
	    public void repetirJuego() {
	        Scanner in = new Scanner(System.in);
	        System.out.println("Desea jugar otra carrera?  Y/N");
	        while (!in.hasNext("[yYnN]")) {
	            System.out.println("Solo se reciben como respuesta Y/N � y/n");
	            in.next();
	        }
	        String jugarOtro = in.next();
	        if (jugarOtro.equals("Y") || jugarOtro.equals("y")) {
	            carrosEnJuego.clear();
	            carrilesEnJuego.clear();
	            Podio podioNuevo = new Podio();
	            podio = podioNuevo;
	            iniciarJuego();

	        }

	    }

	    public void mostrarPodio() {
	        JOptionPane.showMessageDialog(null, "Podio : ");
	        JOptionPane.showMessageDialog(null, " Primer lugar : "+ podio.primerLugar().nombre().getNombre());
	        JOptionPane.showMessageDialog(null, " Segundo lugar : "+ podio.segundoLugar().nombre().getNombre());
	        JOptionPane.showMessageDialog(null, " Tercer lugar : "+ podio.tercerLugar().nombre().getNombre());
	        

	    }

}
