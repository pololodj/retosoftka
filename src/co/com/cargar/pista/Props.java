package co.com.cargar.pista;

import com.cargar.juego.Jugador;

public interface Props {
	 	public Jugador primerLugar();

	    public Jugador segundoLugar();

	    public Jugador tercerLugar();

	    public Boolean estaLleno();

}
